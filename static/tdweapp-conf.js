exports.config = {
  appkey: '5169F435E3DD418E8EEEA2A27A7DFC84',
  appName: 'PLAY PASSPORT',
  versionName: 'PLAYPASSPORT',
  versionCode: '4.1.0',
  wxAppid: 'wxc8fb4c8542ebcfef',
  getLocation: false, // 默认不获取用户位置
  autoOnPullDownRefresh: false, // 默认不统计下拉刷新数据
  autoOnReachBottom: false // 默认不统计页面触底数据
};
